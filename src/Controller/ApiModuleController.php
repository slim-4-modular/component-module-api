<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Controller;

use Paneric\ComponentModuleApi\Interfaces\Action\CreateApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\CreateMultipleApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\DeleteApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\DeleteMultipleApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\GetAllApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\GetAllByApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\GetAllByExtendedApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\GetOneByApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\GetAllPaginatedApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\GetOneByIdApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\UpdateApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\UpdateMultipleApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\ActionListInterface;
use Paneric\Interfaces\Responder\ApiResponderInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ApiModuleController
{
    public function __construct(readonly protected ApiResponderInterface $responder)
    {
    }

    public function create(
        Request $request,
        Response $response,
        CreateApiActionInterface $action,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $response,
            array_merge(
                $action($request->getParsedBody(), $request->getAttribute('route_name')),
                ['list' => $actionList ? $actionList($request) : []]
            )
        );
    }

    public function createMultiple(
        Request $request,
        Response $response,
        CreateMultipleApiActionInterface $action,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $response,
            array_merge(
                $action($request->getParsedBody(), $request->getAttribute('route_name')),
                ['list' => $actionList ? $actionList($request) : []]
            )
        );
    }

    public function delete(
        Request $request,
        Response $response,
        DeleteApiActionInterface $action,
        string $id
    ): Response {
        return ($this->responder)(
            $response,
            $action($id)
        );
    }

    public function deleteMultiple(
        Request $request,
        Response $response,
        DeleteMultipleApiActionInterface $action
    ): Response {
        return ($this->responder)(
            $response,
            $action($request->getParsedBody())
        );
    }

    public function getAll(
        Request $request,
        Response $response,
        GetAllApiActionInterface $action,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $response,
            array_merge(
                $action($request->getQueryParams()),
                ['list' => $actionList ? $actionList($request) : []]
            )
        );
    }

    public function getAllBy(
        Request $request,
        Response $response,
        GetAllByApiActionInterface $action,
        string $field,
        string $value,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $response,
            array_merge(
                $action($request->getQueryParams(), $field, $value),
                ['list' => $actionList ? $actionList($request) : []]
            )
        );
    }

    public function getAllByExtended(
        Request $request,
        Response $response,
        GetAllByExtendedApiActionInterface $action,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $response,
            array_merge(
                $action($request->getQueryParams()),
                ['list' => $actionList ? $actionList($request) : []]
            )
        );
    }

    public function getAllPaginated(
        Request $request,
        Response $response,
        GetAllPaginatedApiActionInterface $action,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $response,
            array_merge(
                $action(
                    $request->getAttribute('pagination'),
                    $request->getQueryParams()
                ),
                ['list' => $actionList ? $actionList($request) : []]
            )
        );
    }

    public function getOneBy(
        Request $request,
        Response $response,
        GetOneByApiActionInterface $action,
        string $field,
        string $value,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $response,
            array_merge(
                $action($field, $value),
                ['list' => $actionList ? $actionList($request) : []]
            )
        );
    }

    public function getOneById(
        Request $request,
        Response $response,
        GetOneByIdApiActionInterface $action,
        string $id,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $response,
            array_merge(
                $action($id),
                ['list' => $actionList ? $actionList($request) : []]
            )
        );
    }

    public function update(
        Request $request,
        Response $response,
        UpdateApiActionInterface $action,
        string $id,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $response,
            array_merge(
                $action($request->getParsedBody(), $id, $request->getAttribute('route_name')),
                ['list' => $actionList ? $actionList($request) : []]
            )
        );
    }

    public function updateMultiple(
        Request $request,
        Response $response,
        UpdateMultipleApiActionInterface $action,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $response,
            array_merge(
                $action($request->getParsedBody(), $request->getAttribute('route_name')),
                ['list' => $actionList ? $actionList($request) : []]
            )
        );
    }
}
