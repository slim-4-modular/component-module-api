<?php

declare(strict_types=1);

use Paneric\ComponentModuleApi\Infrastructure\ModulePersister;
use Paneric\ComponentModuleApi\Infrastructure\ModuleQuery;
use Paneric\ComponentModuleApi\Infrastructure\ModuleRepository;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModulePersisterInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModuleQueryInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\ComponentModuleApi\Responder\ApiResponder;
use Paneric\Interfaces\Responder\ApiResponderInterface;
use Psr\Container\ContainerInterface;
use Paneric\DBAL\Manager;

return [
    ModuleRepositoryInterface::class => static function (ContainerInterface $c): ModuleRepository {
        return new ModuleRepository(
            $c->get(Manager::class),
            $c->get(ModuleConfigInterface::class),
        );
    },
    ModulePersisterInterface::class => static function (ContainerInterface $c): ModulePersister {
        return new ModulePersister(
            $c->get(Manager::class),
            $c->get(ModuleConfigInterface::class),
        );
    },
    ModuleQueryInterface::class => static function (ContainerInterface $c): ModuleQuery {
        return new ModuleQuery(
            $c->get(Manager::class),
            $c->get(ModuleConfigInterface::class),
        );
    },

    ApiResponderInterface::class => static function (): ApiResponder {
        return new ApiResponder();
    },
];
