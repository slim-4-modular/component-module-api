<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Infrastructure\Event;

use Paneric\Interfaces\Event\Event;
use Paneric\Interfaces\Event\EventInterface;

class CreateFailureEvent extends Event implements EventInterface
{
}
