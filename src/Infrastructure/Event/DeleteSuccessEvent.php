<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Infrastructure\Event;

use Paneric\Interfaces\Event\Event;
use Paneric\Interfaces\Event\EventInterface;

class DeleteSuccessEvent extends Event implements EventInterface
{
}
