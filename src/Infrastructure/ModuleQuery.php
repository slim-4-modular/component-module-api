<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Infrastructure;

use Paneric\ComponentModuleApi\Model\Interfaces\ModuleQueryInterface;
use Paneric\DBAL\Manager;
use Paneric\DBAL\Query;

class ModuleQuery extends Query implements ModuleQueryInterface
{
    protected array $configs;

    public function __construct(Manager $manager, ...$configs)
    {
        parent::__construct($manager);

        $this->configs = $configs;
    }
}
