<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Infrastructure;

use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\DBAL\Manager;
use Paneric\DBAL\Repository;

class ModuleRepository extends Repository implements ModuleRepositoryInterface
{
    public function __construct(Manager $manager, ModuleConfigInterface $config)
    {
        parent::__construct($manager);

        $configValues = $config->repository();

        $this->table = $configValues['table'];
        $this->daoClass = $configValues['dto_class'];
        $this->fetchMode = $configValues['fetch_mode'];

        if (!empty($configValues['select_query'])) {
            $this->selectQuery = $configValues['select_query'];
        }
    }
}
