<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Interfaces\Action;

interface GetAllApiActionInterface
{
    public function __invoke(array $queryParams): array;
}
