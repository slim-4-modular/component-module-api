<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Interfaces\Action;

interface UpdateApiActionInterface
{
    public function __invoke(array $attributes, string $id, string $routeName): ?array;
}
