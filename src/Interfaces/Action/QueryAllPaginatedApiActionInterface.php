<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Interfaces\Action;

use Psr\Http\Message\ServerRequestInterface as Request;

interface QueryAllPaginatedApiActionInterface
{
    public function __invoke(Request $request, string $page): array;
    public function getStatus(): int;
}
