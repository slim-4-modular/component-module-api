<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Interfaces\Action;

interface DeleteApiActionInterface
{
    public function __invoke(string $id): ?array;
}
