<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Interfaces\Action;

use Psr\Http\Message\ServerRequestInterface as Request;

interface GetAllJoinedApiActionInterface
{
    public function __invoke(Request $request): array;
    public function getStatus(): int;
}
