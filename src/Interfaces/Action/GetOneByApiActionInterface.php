<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Interfaces\Action;

interface GetOneByApiActionInterface
{
    public function __invoke(string $field, string $value): ?array;
}
