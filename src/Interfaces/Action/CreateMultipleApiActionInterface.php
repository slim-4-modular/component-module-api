<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Interfaces\Action;

interface CreateMultipleApiActionInterface
{
    public function __invoke(array $attributes, string $routeName): ?array;
}
