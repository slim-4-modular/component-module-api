<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Interfaces\Action;

interface GetAllPaginatedApiActionInterface
{
    public function __invoke(array $pagination, array $queryParams): array;
}
