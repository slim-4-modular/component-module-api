<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Interfaces\Action;

interface GetAllByExtendedApiActionInterface
{
    public function __invoke(array $attributes): array;
}
