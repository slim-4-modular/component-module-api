<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Interfaces\Action;

interface GetAllByApiActionInterface
{
    public function __invoke(array $queryParams, string $field, string $value): array;
}
