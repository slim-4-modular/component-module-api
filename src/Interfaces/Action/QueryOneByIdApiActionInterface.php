<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Interfaces\Action;

use Psr\Http\Message\ServerRequestInterface as Request;

interface QueryOneByIdApiActionInterface
{
    public function __invoke(Request $request, string $id): ?array;
    public function getStatus(): int;
}
