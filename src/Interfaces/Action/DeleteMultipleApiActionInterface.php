<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Interfaces\Action;

interface DeleteMultipleApiActionInterface
{
    public function __invoke(array $attributes): ?array;
}
