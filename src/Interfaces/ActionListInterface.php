<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Interfaces;

use Psr\Http\Message\RequestInterface as Request;

interface ActionListInterface
{
    public function __invoke(Request $request = null): array;
}
