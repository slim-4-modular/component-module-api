<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action;

use Paneric\ComponentModuleApi\Exceptions\ValidationException;
use Paneric\ComponentModuleApi\Infrastructure\Event\UpdateFailureEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\BeforeUpdateEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\UpdateSuccessEvent;
use Paneric\ComponentModuleApi\Interfaces\Action\UpdateMultipleApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModulePersisterInterface;
use Paneric\CSRTriad\Action;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class UpdateMultipleApiAction extends Action implements UpdateMultipleApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->updateMultiple();
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(array $attributes, string $routeName): ?array
    {
        $collection = $this->createCollectionFromAttributes(
            $attributes,
            $this->config['dto_class'],
            true
        );

        $findByCriteria = $this->config['find_by_criteria'];
        $updateUniqueCriteria = $this->config['update_unique_criteria'];

        $criteriaSelect = $findByCriteria($collection);
        $criteriaUpdate = $updateUniqueCriteria($collection);

        try {
            (new $this->config['vld_class']())($collection, $routeName);

            $this->eventDispatcher->dispatch(new BeforeUpdateEvent($collection));
            $updateSuccessEvent = new UpdateSuccessEvent($collection);
            $collectionIds = $this->getCollectionIds($collection);

            $collection = $this->adapter->updateUniqueMultiple(
                $criteriaSelect,
                $criteriaUpdate,
                $collection
            );

            if (empty($collection)) {
                $this->eventDispatcher->dispatch($updateSuccessEvent);

                return [
                    'status' => 200,
                    'body' => ['id' => $collectionIds]
                ];
            }

            $this->eventDispatcher->dispatch(new UpdateFailureEvent($collection));

            return [
                'status' => 400,
                'error' => 'db_update_multiple_unique_error',
                'body' => $this->convertCollection($collection)
            ];
        } catch (ValidationException $e) {
            unset($attributes['csrf_hash']);
            return [
                'status' => 400,
                'error' => 'validation_update_multiple_error',
                'validation' => $e->getReport(),
                'body' => $attributes,
            ];
        }
    }
}
