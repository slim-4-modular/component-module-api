<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action;

use Paneric\ComponentModuleApi\Infrastructure\Event\DeleteFailureEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\BeforeDeleteEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\DeleteSuccessEvent;
use Paneric\ComponentModuleApi\Interfaces\Action\DeleteApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModulePersisterInterface;
use Paneric\CSRTriad\Action;
use Psr\EventDispatcher\EventDispatcherInterface;

class DeleteApiAction extends Action implements DeleteApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected EventDispatcherInterface $eventDispatcher;

    protected int $status;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->delete();
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(string $id): ?array
    {
        $deleteByCriteria = $this->config['delete_by_criteria'];

        $this->eventDispatcher->dispatch(new BeforeDeleteEvent($id));

        if ($this->adapter->delete($deleteByCriteria(['id' => $id])) === 0) {
            $this->eventDispatcher->dispatch(new DeleteFailureEvent($id));

            return  [
                'status' => 400,
                'error' => 'db_delete_error',
                'body' => ['id' => $id]
            ];
        }

        $this->eventDispatcher->dispatch(new DeleteSuccessEvent($id));

        return  [
            'status' => 200,
            'body' => ['id' => $id]
        ];
    }
}
