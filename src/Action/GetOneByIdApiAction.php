<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action;

use Paneric\ComponentModuleApi\Interfaces\Action\GetOneByIdApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\CSRTriad\Action;

class GetOneByIdApiAction extends Action implements GetOneByIdApiActionInterface
{
    protected ModuleRepositoryInterface $adapter;
    protected array $config;

    protected int $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ModuleConfigInterface $config
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->getOneById();
    }

    public function __invoke(string $id): ?array
    {
        $findBySingleCriteria = $this->config['find_by_single_criteria'];

        $dto = $this->adapter->findOneBy($findBySingleCriteria('id', $id));

        if ($dto ===  null) {
            return [
                'status' => 400,
                'error' => 'resource_not_found'
            ];
        }

        $dtos = $this->arrangeObjectsCollectionById([$dto], true);

        return [
            'status' => 200,
            'body' => array_shift(
                $dtos
            ),
        ];
    }
}
