<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action;

use Paneric\ComponentModuleApi\Interfaces\ActionListInterface;
use Psr\Http\Message\RequestInterface as Request;

class ActionList implements ActionListInterface
{
    protected array $actions;
    protected array $subData;

    public function __construct(array $actions)
    {
        $this->actions = $actions;
    }

    public function __invoke(Request $request = null): array
    {
        $this->subData = [];

        foreach ($this->actions as $action) {
            $this->merge($action, $request);
        }

        return $this->subData;
    }

    private function merge(mixed $action, Request $request = null): void
    {
        if ($request === null) {
            $actionData = $action();
        } else {
            $actionData = $action($request);
        }
        $this->subData[] = $actionData;
    }
}
