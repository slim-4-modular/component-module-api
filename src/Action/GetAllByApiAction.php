<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action;

use Paneric\ComponentModuleApi\Interfaces\Action\GetAllByApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\CSRTriad\Action;

class GetAllByApiAction extends Action implements GetAllByApiActionInterface
{
    protected ModuleRepositoryInterface $adapter;
    protected array $config;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ModuleConfigInterface $config
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->getAllBy();
    }

    public function __invoke(array $queryParams, string $field, string $value): array
    {
        $value = urldecode($value);

        return [
            'status' => 200,
            'body' => $this->getAllBy($queryParams, $field, $value),
        ];
    }

    public function getAllBy(array $queryParams, string $field, string $value): array
    {
        $orderBySingle = $this->config['order_by_single'];
        $findBySingleCriteria = $this->config['find_by_single_criteria'];

        $collection = $this->adapter->findBy(
            $findBySingleCriteria($field, $value),
            $orderBySingle($queryParams['field'] ?? 'id')
        );

        return $this->arrangeObjectsCollection($collection, true);
    }
}
