<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action;

use Paneric\ComponentModuleApi\Interfaces\Action\GetAllByExtendedApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\CSRTriad\Action;

class GetAllByExtendedApiAction extends Action implements GetAllByExtendedApiActionInterface
{
    protected ModuleRepositoryInterface $adapter;
    protected array $config;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ModuleConfigInterface $config
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->getAllByExt();
    }

    public function __invoke(array $attributes): array
    {
        $findByCriteria = $this->config['find_by_criteria'];
        $criteria = $findByCriteria($attributes);

        $orderByCriteria = $this->config['order_by_criteria'];
        $orderBy = $orderByCriteria($attributes);

        return [
            'status' => 200,
            'body' => $this->getAllByExtended($criteria, $orderBy),
        ];
    }

    protected function getAllByExtended(array $criteria, array $orderBy): array
    {
        $collection = $this->adapter->findBy(
            $criteria,
            $orderBy
        );

        return $this->arrangeObjectsCollection($collection, true);
    }
}
