<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action;

use Paneric\ComponentModuleApi\Interfaces\Action\GetOneByApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\CSRTriad\Action;

class GetOneByApiAction extends Action implements GetOneByApiActionInterface
{
    protected ModuleRepositoryInterface $adapter;
    protected array $config;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ModuleConfigInterface $config
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->getOneBy();
    }

    public function __invoke(string $field, string $value): ?array
    {
        $value = urldecode($value);

        $findBySingleCriteria = $this->config['find_by_single_criteria'];

        $dto = $this->adapter->findOneBy($findBySingleCriteria($field, $value));

        if ($dto ===  null) {
            return [
                'status' => 400,
                'error' => 'resource_not_found'
            ];
        }

        $dtos = $this->arrangeObjectsCollectionById([$dto], true);

        return [
            'status' => 200,
            'body' => array_shift(
                $dtos
            ),
        ];
    }
}
