<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action\Config;

use Closure;
use JetBrains\PhpStorm\ArrayShape;

trait ModuleConfigTrait
{
    protected function findBySingleCriteria(): closure
    {
        return function (string $field, int|string $value): array {
            return [$this->prefix . $field => urldecode($value)];
        };
    }

    protected function findByCriteria(): closure
    {
        return function (array $field, array $value): array {
            $criteria = [];
            foreach ($field as $idx => $fld) {
                $criteria[$this->prefix . $fld] = urldecode($value[$idx]);
            }
            return $criteria;
        };
    }

    protected function orderBySingle(): closure
    {
        return function (string $orderByField, string $order = 'ASC'): array {
            return [$this->prefix . $orderByField => $order];
        };
    }

    #[ArrayShape([
        'find_by_single_criteria' => Closure::class
    ])]
    public function getOneById(): array
    {
        return [
            'find_by_single_criteria' => $this->findBySingleCriteria(),
        ];
    }

    #[ArrayShape([
        'find_by_single_criteria' => Closure::class
    ])]
    public function getOneBy(): array
    {
        return [
            'find_by_single_criteria' => $this->findBySingleCriteria(),
        ];
    }

    #[ArrayShape([
        'order_by_single' => Closure::class
    ])]
    public function getAll(): array
    {
        return [
            'order_by_single' => $this->orderBySingle(),
        ];
    }

    #[ArrayShape([
        'order_by_single' => Closure::class,
        'find_by_single_criteria' => Closure::class,
    ])]
    public function getAllBy(): array
    {
        return [
            'order_by_single' => $this->orderBySingle(),
            'find_by_single_criteria' => $this->findBySingleCriteria(),
        ];
    }

    #[ArrayShape([
        'find_by_criteria' => Closure::class,
        'order_by_criteria' => Closure::class
    ])]
    public function getAllByExt(): array
    {
        return [
            'find_by_criteria' => function (array $attributes, string $local = null): array {
                if (empty($attributes['find']) || empty($attributes['fvalue'])) {
                    return [];
                }

                return $this->prepareExtendedConditions($attributes['find'], $attributes['fvalue']);
            },
            'order_by_criteria' => function (array $attributes, string $orderByField = null): array {
                if (empty($attributes['orderby']) || empty($attributes['ovalue'])) {
                    return [];
                }

                return $this->prepareExtendedConditions($attributes['orderby'], $attributes['ovalue']);
            },
        ];
    }

    protected function prepareExtendedConditions(array|string $fields, array $values): array
    {
        $criteria = [];
        if (is_array($fields)) {
            foreach ($fields as $key => $field) {
                $criteria[$this->prefix . $field] = $values[$key];
            }
            return $criteria;
        }
        foreach ($values as $ref => $value) {
            $criteria[$this->prefix . $fields][$ref] = $value;
        }
        return $criteria;
    }

    #[ArrayShape([
        'order_by_single' => Closure::class
    ])]
    public function getAllJoined(): array
    {
        return [
            'order_by_single' => $this->orderBySingle(),
        ];
    }

    #[ArrayShape([
        'order_by_single' => Closure::class
    ])]
    public function getAllPaginated(): array
    {
        return [
            'order_by_single' => $this->orderBySingle(),
        ];
    }

    #[ArrayShape([
        'dto_class' => "string",
        'vld_class' => "string",
        'create_unique_criteria' => Closure::class,
        'string_id' => "bool"
    ])]
    public function create(): array
    {
        return [
            'dto_class' => $this->daoClass,
            'vld_class' => $this->vldClass,
            'create_unique_criteria' => function (array $attributes): array {
                foreach ($this->uniqueKeys as $uniqueKey) {
                    $createUniqueCriteria[$this->prefix . $uniqueKey] = $attributes[$uniqueKey];
                }

                return $createUniqueCriteria;
            },
            'string_id' => $this->stringId,
        ];
    }

    #[ArrayShape([
        'delete_by_criteria' => Closure::class
    ])]
    public function delete(): array
    {
        return [
            'delete_by_criteria' => function (array $attributes): array {
                $deleteByCriteria = [];

                foreach ($attributes as $key => $value) {
                    $deleteByCriteria[$this->prefix  . $key] = $value;
                }

                return $deleteByCriteria;
            },
        ];
    }

    #[ArrayShape([
        'dto_class' => "string",
        'vld_class' => "string",
        'delete_by_criteria' => Closure::class
    ])]
    public function deleteMultiple(): array
    {
        return [
            'dto_class' => $this->daoClass,
            'vld_class' => $this->vldClass,
            'delete_by_criteria' => function (array $collection): array {
                $deleteByCriteria = [];

                foreach ($collection as $index => $dao) {
                    $id = $dao->getId();
                    if ($id !== null) {
                        $deleteByCriteria[$index][$this->prefix . 'id'] = $id;
                        continue;
                    }

                    foreach ($this->uniqueKeys as $uniqueKey) {
                        $deleteByCriteria[$index][$this->prefix . $uniqueKey] =
                            $dao->{'get' . ucfirst($this->hydrateField($uniqueKey))}();
                    }
                }

                return $deleteByCriteria;
            },
        ];
    }

    protected function hydrateField(string $uniqueKey): string
    {
        if (str_contains($uniqueKey, '_')) {
            return str_replace(' ', '', ucwords(str_replace('_', ' ', $uniqueKey)));
        }
        return $uniqueKey;
    }

    protected function prepareCreateWhereCondition(): string
    {
        return $this->prepareWhereCondition(false);
    }

    protected function prepareUpdateWhereCondition(): string
    {
        return $this->prepareWhereCondition(true);
    }

    protected function prepareWhereCondition(bool $withNotIn): string
    {
        $whereCondition = '';
        if (!empty($this->uniqueKeys)) {
            foreach ($this->uniqueKeys as $ii => $uniqueKey) {
                if ($ii === 0) {
                    $condition = sprintf(
                        ' WHERE %s=:%s',
                        $this->prefix . $uniqueKey,
                        $this->prefix . $uniqueKey
                    );
                    $whereCondition .= $condition;
                    continue;
                }
                $condition = sprintf(
                    ' AND %s=:%s',
                    $this->prefix . $uniqueKey,
                    $this->prefix . $uniqueKey
                );
                $whereCondition .= $condition;
            }
        }

        if (!$withNotIn) {
            return $whereCondition;
        }

        $condition = sprintf(' AND %sid NOT IN (:%sid)', $this->prefix, $this->prefix);
        $whereCondition .= $condition;

        return $whereCondition;
    }
}
