<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action\Config;

trait ModuleConfigGhostTrait
{
    public function getOneById(): array
    {
        return [];
    }

    public function getOneBy(): array
    {
        return [];
    }

    public function getAll(): array
    {
        return [];
    }

    public function getAllBy(): array
    {
        return [];
    }

    public function getAllByExt(): array
    {
        return [];
    }

    public function getAllPaginated(): array
    {
        return [];
    }

    public function create(): array
    {
        return [];
    }

    public function createMultiple(): array
    {
        return [];
    }

    public function update(): array
    {
        return [];
    }

    public function updateMultiple(): array
    {
        return [];
    }

    public function delete(): array
    {
        return [];
    }

    public function deleteMultiple(): array
    {
        return [];
    }

    public function repository(): array
    {
        return [];
    }

    public function persister(): array
    {
        return [];
    }

    public function query(): array
    {
        return [];
    }
}
