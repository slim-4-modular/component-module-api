<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action;

use Paneric\ComponentModuleApi\Exceptions\ValidationException;
use Paneric\ComponentModuleApi\Infrastructure\Event\CreateFailureEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\BeforeCreateEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\CreateSuccessEvent;
use Paneric\ComponentModuleApi\Interfaces\Action\CreateApiActionInterface;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModulePersisterInterface;
use Paneric\CSRTriad\Action;
use Psr\EventDispatcher\EventDispatcherInterface;

class CreateApiAction extends Action implements CreateApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected GuardInterface $guard;
    protected EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        GuardInterface $guard,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->create();
        $this->guard = $guard;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(array $attributes, string $routeName): ?array
    {
        $dto = new $this->config['dto_class']();
        $dto->hydrate($attributes);

        $createUniqueCriteria = $this->config['create_unique_criteria'];

        try {
            (new $this->config['vld_class']())($dto, $routeName);

            $stringId = $this->config['string_id'];

            if ($stringId) {
                $dto->setId($this->guard->setUniqueId());
            }

            $this->eventDispatcher->dispatch(new BeforeCreateEvent($dto));
            $createResult = $this->adapter->createUnique($createUniqueCriteria($attributes), $dto);

            if ($createResult !== null) {
                $this->eventDispatcher->dispatch(new CreateSuccessEvent($dto));
                return [
                    'status' => 201,
                    'body' => [
                        'id' => $stringId ? $dto->getId() : $createResult,
                    ],
                ];
            }

            $this->eventDispatcher->dispatch(new CreateFailureEvent($dto));

            return [
                'status' => 400,
                'error' => 'db_create_unique_error',
                'body' => $attributes
            ];
        } catch (ValidationException $e) {
            unset($attributes['csrf_hash']);
            return [
                'status' => 400,
                'error' => 'validation_create_error',
                'validation' => $e->getReport(),
                'body' => $attributes,
            ];
        }
    }
}
