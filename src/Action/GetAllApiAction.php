<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action;

use Paneric\ComponentModuleApi\Interfaces\Action\GetAllApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\CSRTriad\Action;

class GetAllApiAction extends Action implements GetAllApiActionInterface
{
    protected ModuleRepositoryInterface $adapter;
    protected array $config;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ModuleConfigInterface $config
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->getAll();
    }

    public function __invoke(array $queryParams): array
    {
        return [
            'status' => 200,
            'body' => $this->getAll($queryParams),
        ];
    }

    public function getAll(array $queryParams): array
    {
        $orderBySingle = $this->config['order_by_single'];

        $collection = $this->adapter->findBy(
            [],
            $orderBySingle($queryParams['field'] ?? 'id')
        );

        return $this->arrangeObjectsCollection($collection, true);
    }
}
