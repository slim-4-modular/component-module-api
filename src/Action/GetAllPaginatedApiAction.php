<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action;

use Paneric\ComponentModuleApi\Interfaces\Action\GetAllPaginatedApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\CSRTriad\Action;

class GetAllPaginatedApiAction extends Action implements GetAllPaginatedApiActionInterface
{
    protected ModuleRepositoryInterface $adapter;
    protected array $config;

    protected int $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ModuleConfigInterface $config
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->getAllPaginated();
    }

    public function __invoke(array $pagination, array $queryParams): array
    {
        $this->status = 200;

        $orderBySingle = $this->config['order_by_single'];

        $collection = $this->adapter->findBy(
            [],
            $orderBySingle($queryParams['field'] ?? 'id'),
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            'status' => 200,
            'body' => $this->arrangeObjectsCollection($collection, true),
            'pagination' => $pagination,
        ];
    }
}
