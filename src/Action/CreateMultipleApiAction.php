<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action;

use Paneric\ComponentModuleApi\Exceptions\ValidationException;
use Paneric\ComponentModuleApi\Infrastructure\Event\CreateFailureEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\BeforeCreateEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\CreateSuccessEvent;
use Paneric\ComponentModuleApi\Interfaces\Action\CreateMultipleApiActionInterface;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModulePersisterInterface;
use Paneric\CSRTriad\Action;
use Psr\EventDispatcher\EventDispatcherInterface;

class CreateMultipleApiAction extends Action implements CreateMultipleApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected GuardInterface $guard;
    protected EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        GuardInterface $guard,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->createMultiple();
        $this->guard = $guard;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(array $attributes, string $routeName): ?array
    {
        $collection = $this->createCollectionFromAttributes(
            $attributes,
            $this->config['dto_class'],
            true
        );

        $createUniqueCriteria = $this->config['create_unique_criteria'];

        try {
            (new $this->config['vld_class']())($collection, $routeName);

            $stringId = $this->config['string_id'];

            if ($stringId) {
                $guard = $this->guard;
                array_walk($collection, static function ($item) use ($guard) {
                    $item->setId($guard->setUniqueId());
                });
            }

            $criteria = $createUniqueCriteria($collection);

            $this->eventDispatcher->dispatch(new BeforeCreateEvent($collection));
            $createSuccessEvent = new CreateSuccessEvent($collection);
            if ($stringId) {
                $collectionIds = $this->getCollectionIds($collection);
            }

            $collection = $this->adapter->createUniqueMultiple($criteria, $collection);

            if (empty($collection['data_objects'])) {
                $this->eventDispatcher->dispatch($createSuccessEvent);

                $collectionIds = $stringId ? $collectionIds : $collection['last_inserted_ids'];

                return [
                    'status' => 201,
                    'body' => ['id' => $collectionIds]
                ];
            }

            $this->eventDispatcher->dispatch(new CreateFailureEvent($collection['data_objects']));

            return [
                'status' => 400,
                'error' => 'db_create_multiple_error',
                'body' => $this->convertCollection($collection['data_objects'])
            ];
        } catch (ValidationException $e) {
            unset($attributes['csrf_hash']);
            return [
                'status' => 400,
                'error' => 'validation_create_multiple_error',
                'validation' => $e->getReport(),
                'body' => $attributes,
            ];
        }
    }
}
