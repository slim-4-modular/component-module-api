<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action;

use Exception;
use Paneric\ComponentModuleApi\Exceptions\ValidationException;
use Paneric\ComponentModuleApi\Infrastructure\Event\UpdateFailureEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\BeforeUpdateEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\UpdateSuccessEvent;
use Paneric\ComponentModuleApi\Interfaces\Action\UpdateApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModulePersisterInterface;
use Paneric\CSRTriad\Action;
use Psr\EventDispatcher\EventDispatcherInterface;
use Throwable;

class UpdateApiAction extends Action implements UpdateApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->update();
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(array $attributes, string $id, string $routeName): ?array
    {
        $dto = new $this->config['dto_class']();
        $dto->hydrate($attributes);

        $findOneByCriteria = $this->config['find_one_by_criteria'];
        $updateUniqueCriteria = $this->config['update_unique_criteria'];

        try {
            (new $this->config['vld_class']())($dto, $routeName);

            $this->eventDispatcher->dispatch(new BeforeUpdateEvent($dto));
            $updateResult = $this->adapter->updateUnique(
                $findOneByCriteria($dto, $id),
                $updateUniqueCriteria($id),
                $dto
            );

            if ($updateResult !== null) {
                $this->eventDispatcher->dispatch(new UpdateSuccessEvent($dto));

                return [
                    'status' => 200,
                    'body' => ['id' => $id],
                ];
            }

            $this->eventDispatcher->dispatch(new UpdateFailureEvent($dto));

            return [
                'status' => 400,
                'error' => 'db_update_unique_error',
                'body' => array_merge(['id' => $id], $attributes)
            ];
        } catch (ValidationException $e) {
            unset($attributes['csrf_hash']);
            return [
                'status' => 400,
                'error' => 'validation_update_error',
                'validation' => $e->getReport(),
                'body' => $attributes,
            ];
        }
    }
}
