<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action;

use Paneric\ComponentModuleApi\Infrastructure\Event\BeforeDeleteEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\DeleteFailureEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\DeleteSuccessEvent;
use Paneric\ComponentModuleApi\Interfaces\Action\DeleteByApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModulePersisterInterface;
use Paneric\CSRTriad\Action;
use Psr\EventDispatcher\EventDispatcherInterface;

class DeleteByApiAction extends Action implements DeleteByApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected EventDispatcherInterface $eventDispatcher;

    protected int $status;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->delete();
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(string $field, string $value): ?array
    {
        $value = urldecode($value);

        $deleteByCriteria = $this->config['delete_by_criteria'];

        $this->eventDispatcher->dispatch(new BeforeDeleteEvent($value));

        if ($this->adapter->delete($deleteByCriteria([$field => $value])) === 0) {
            $this->eventDispatcher->dispatch(new DeleteFailureEvent($value));

            return  [
                'status' => 400,
                'error' => 'db_delete_error',
                'body' => [$field => $value]
            ];
        }

        $this->eventDispatcher->dispatch(new DeleteSuccessEvent($value));

        return  [
            'status' => 200,
            'body' => [$field => $value]
        ];
    }
}
