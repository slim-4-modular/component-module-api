<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Action;

use Paneric\ComponentModuleApi\Infrastructure\Event\DeleteFailureEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\BeforeDeleteEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\DeleteSuccessEvent;
use Paneric\ComponentModuleApi\Interfaces\Action\DeleteMultipleApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModulePersisterInterface;
use Paneric\CSRTriad\Action;
use Psr\EventDispatcher\EventDispatcherInterface;

class DeleteMultipleApiAction extends Action implements DeleteMultipleApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->deleteMultiple();
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(array $attributes): ?array
    {
        $collection = $this->createCollectionFromAttributes(
            $attributes,
            $this->config['dto_class'],
            true
        );

        $deleteByCriteria = $this->config['delete_by_criteria'];

        $this->eventDispatcher->dispatch(new BeforeDeleteEvent($collection));
        $deleteSuccessEvent = new DeleteSuccessEvent($collection);
        $collectionIds = $this->getCollectionIds($collection);

        $collection = $this->adapter->deleteMultiple(
            $deleteByCriteria($collection),
            $collection
        );

        if (empty($collection)) {
            $this->eventDispatcher->dispatch($deleteSuccessEvent);

            return [
                'status' => 200,
                'body' => ['id' => $collectionIds],
            ];
        }

        $this->eventDispatcher->dispatch(new DeleteFailureEvent($collection));

        return [
            'status' => 400,
            'error' => 'db_delete_multiple_error',
            'body' => ['id' => $collectionIds],
        ];
    }
}
