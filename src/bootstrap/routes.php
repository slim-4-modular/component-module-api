<?php

declare(strict_types=1);

use Paneric\ComponentModuleApi\Action\CreateApiAction;
use Paneric\ComponentModuleApi\Action\CreateMultipleApiAction;
use Paneric\ComponentModuleApi\Action\DeleteApiAction;
use Paneric\ComponentModuleApi\Action\DeleteMultipleApiAction;
use Paneric\ComponentModuleApi\Action\GetAllApiAction;
use Paneric\ComponentModuleApi\Action\GetAllByApiAction;
use Paneric\ComponentModuleApi\Action\GetAllPaginatedApiAction;
use Paneric\ComponentModuleApi\Action\GetAllByExtendedApiAction;
use Paneric\ComponentModuleApi\Action\GetOneByIdApiAction;
use Paneric\ComponentModuleApi\Action\GetOneByApiAction;
use Paneric\ComponentModuleApi\Action\UpdateApiAction;
use Paneric\ComponentModuleApi\Action\UpdateMultipleApiAction;
use Paneric\ComponentModuleApi\Controller\ApiModuleController;
use Paneric\Pagination\PaginationMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($slim, $container, $module, $proxyPrefix)) {
    $routePrefix = '/' . $module;
    $routeNamePrefix = $module;

    if (!empty($proxyPrefix)) {
        $routePrefix = $proxyPrefix  . '/' . $module;
        $routeNamePrefix = str_replace('/', '', $proxyPrefix) . '.' . $module;
        $routeNamePrefix = str_replace('-api', '', $routeNamePrefix);
    }

    try {
        $slim->post($routePrefix . '/add', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->create(
                $request,
                $response,
                $this->get(CreateApiAction::class)
            );
        })->setName($routeNamePrefix . '.add');

        $slim->post($routePrefix . 's/add', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->createMultiple(
                $request,
                $response,
                $this->get(CreateMultipleApiAction::class)
            );
        })->setName($routeNamePrefix . 's.add');


        $slim->delete($routePrefix . '/remove/{id}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->delete(
                $request,
                $response,
                $this->get(DeleteApiAction::class),
                $args['id']
            );
        })->setName($routeNamePrefix . '.remove');

        $slim->delete($routePrefix . 's/remove', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->deleteMultiple(
                $request,
                $response,
                $this->get(DeleteMultipleApiAction::class)
            );
        })->setName($routeNamePrefix . 's.remove');


        $slim->get($routePrefix . 's/get', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->getAll(
                $request,
                $response,
                $this->get(GetAllApiAction::class)
            );
        })->setName($routeNamePrefix . 's.get');

        $slim->get($routePrefix . 's/get/{field}/{value}', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApiModuleController::class)->getAllBy(
                $request,
                $response,
                $this->get(GetAllByApiAction::class),
                $args['field'],
                $args['value']
            );
        })->setName($routeNamePrefix . 's.get-by');

        // /auth-api/actions/get-ext?find=ref&fvalue[]=add&fvalue[]=remove
        // /auth-api/actions/get-ext?find[]=ref&find[]=id&fvalue[]=add&fvalue[]=1
        $slim->get($routePrefix . 's/get-ext', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->getAllByExtended(
                $request,
                $response,
                $this->get(GetAllByExtendedApiAction::class)
            );
        })->setName($routeNamePrefix . 's.get-ext');

        $slim->get($routePrefix . 's/get-paginated[/{page}]', function (
            Request $request,
            Response $response
        ) {
            return $this->get(ApiModuleController::class)->getAllPaginated(
                $request,
                $response,
                $this->get(GetAllPaginatedApiAction::class)
            );
        })->setName($routeNamePrefix . 's.get-paginated')
            ->addMiddleware($container->get(PaginationMiddleware::class));

        $slim->get($routePrefix . '/get/{field}/{value}', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApiModuleController::class)->getOneBy(
                $request,
                $response,
                $this->get(GetOneByApiAction::class),
                $args['field'],
                $args['value']
            );
        })->setName($routeNamePrefix . '.get-by');

        $slim->get($routePrefix . '/get/{id}', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApiModuleController::class)->getOneById(
                $request,
                $response,
                $this->get(GetOneByIdApiAction::class),
                $args['id']
            );
        })->setName($routeNamePrefix . '.get-by-id');

        $slim->put($routePrefix . '/edit/{id}', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApiModuleController::class)->update(
                $request,
                $response,
                $this->get(UpdateApiAction::class),
                $args['id']
            );
        })->setName($routeNamePrefix . '.edit');

        $slim->put($routePrefix . 's/edit', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->updateMultiple(
                $request,
                $response,
                $this->get(UpdateMultipleApiAction::class)
            );
        })->setName($routeNamePrefix . 's.edit');
    } catch (Exception $e) {
    }
}
