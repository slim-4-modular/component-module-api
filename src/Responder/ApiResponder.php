<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Responder;

use JsonException;
use Paneric\Interfaces\Responder\ApiResponderInterface;
use Psr\Http\Message\ResponseInterface as Response;
use RuntimeException;

class ApiResponder implements ApiResponderInterface
{
    public function __invoke(Response $response, array $data = [], ...$params): Response
    {
        return $this->jsonResponse($response, $data, $params[0] ?? 200, $params[1] ?? 0);
    }

    protected function jsonResponse(
        Response $response,
        array $data,
        int $status,
        int $encodingOptions
    ): Response {
        try {
            $status = $data['status'] ?? $status;
            $json = false;

            $response->getBody()->write(
                $json = json_encode($data, JSON_THROW_ON_ERROR | $encodingOptions, 512)
            );
        } catch (JsonException $e) {
            echo sprintf(
                '%s%s%s%s',
                $e->getFile() . "\n",
                $e->getLine() . "\n",
                $e->getMessage() . "\n",
                $e->getTraceAsString() . "\n"
            );
        }

        if ($json === false) {
            throw new RuntimeException(json_last_error_msg(), json_last_error());
        }

        $response = $response->withHeader('Content-Type', 'application/json;charset=utf-8');

        return $response->withStatus($status);
    }
}
