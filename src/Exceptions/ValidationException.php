<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Exceptions;

use Exception;
use Throwable;

class ValidationException extends Exception
{
    public function __construct(
        protected array $report = [],
        string $message = '',
        int $code = 0,
        Throwable|null $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }

    public function getReport(): array
    {
        return $this->report;
    }
}
