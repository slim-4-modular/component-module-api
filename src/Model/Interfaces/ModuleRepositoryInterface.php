<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Model\Interfaces;

use Paneric\Interfaces\DataObject\DataObjectInterface;

interface ModuleRepositoryInterface
{
    public function setSelectQuery(string $selectQuery): void;

    public function setFetchMode(int $fetchMode): void;

    public function findOneBy(array $criteria): null|DataObjectInterface|array;

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array;

    public function getRowsNumber(array $criteria = null): int;
}
