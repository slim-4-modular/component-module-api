<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Model\Interfaces;

use Paneric\ComponentModuleApi\Exceptions\ValidationException;
use Paneric\Interfaces\DataObject\DataObjectInterface;

interface ValidatorInterface
{
    /**
     * @throws ValidationException
     */
    public function __invoke(DataObjectInterface|array $dao, string $routeName): void;
}
