<?php

declare(strict_types=1);

namespace Paneric\ComponentModuleApi\Model;

use Paneric\ComponentModuleApi\Exceptions\ValidationException;
use Paneric\Interfaces\DataObject\DataObjectInterface;

abstract class Validator
{
    protected array $messages = [];
    /**
     * @throws ValidationException
     */
    public function __invoke(DataObjectInterface|array $dao, string $routeName): void
    {
        if (is_array($dao)) {
            foreach ($dao as $index => $d) {
                $this->validate($d->convert(), $routeName, $index);
            }
            return;
        }

        $this->validate($dao->convert(), $routeName);

        if (!empty($this->messages)) {
            throw new ValidationException($this->messages);
        }
    }

    protected function validate(array $attributes, string $routeName, int $index = null): void
    {
        $validators = $this->routeValidators[$routeName];

        foreach ($validators as $field) {
            $this->{$field . 'Validator'}($attributes, $index);
        }
    }

    protected function setMessages(string $field, string $message, ?int $index): void
    {
        if ($index === null) {
            $this->messages[$field] = $message;
        } else {
            $this->messages[$field][$index] = $message;
        }
    }
}
